<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
 xml:lang="es"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<title>Monitoreo de Indicadores de Sequ&#237;a</title>
<link rel="stylesheet" type="text/css" href="../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="index.html?Set-Language=en" />
<link rel="canonical" href="index.html" />
<meta property="maproom:Sort_Id" content="a04" />
<link class="carryLanguage" rel="home" href="http://www.dga.cl/" title="DGA" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="../../maproom//navmenu.json" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy"
href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#monitoring" />
<link rel="term:icon"
href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.Analysis/.UNIFIED_PRCP/a:/.SPI%5B%28.%29%28SPI1%29//var/parameter%5Dconcat/interp/:a:/.Countrymask/.mask/mul//plotlast/3.5/def//plotfirst/-3.5/:a/def/DM_SPI_2p5_colors/T/%28Dec%202013%29VALUE%5BT%5Daverage//name//SPI/def/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//Y/-56/-16/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef//layers%5B//SPI//lakes//coasts_gaz//countries_gaz//states_gaz%5Dpsdef/++Y/-55.25/-17.25/plotrange+//XOVY+null+psdef//plotaxislength+550+psdef//plotborder+72+psdef+.gif" />
<script type="text/javascript" src="../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../maproom/unesco.js"></script>
</head>
<body>

<form name="pageform" id="pageform">
<input class="carryLanguage carryup" name="Set-Language" type="hidden" />
<input class="carry titleLink itemImage" name="bbox" type="hidden" />
</form>
<div class="controlBar">
           <fieldset class="navitem"> 
                <legend>Chile</legend> 
                    <a rev="section" class="navlink carryup" href="/maproom/">Observatorio DGA</a>
            </fieldset> 
           <fieldset class="navitem"> 
                <legend>Chile</legend> 
                     <span class="navtext">Monitoreo de Sequ&#237;a</span>
            </fieldset> 
            <fieldset class="navitem">
                <legend>Región</legend>
                <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsChile.json"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">Chile</option>
                <optgroup class="template" label="Región">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup>
                </select>
           </fieldset>
</div>
<div>
 <div id="content" class="searchDescription">
<h2 property="term:title">Monitoreo de Sequ&#237;a</h2>
<p align="left" property="term:description">
Mapas para el monitoreo de sequ&#237;a a trav&#233;s de indicadores de sequ&#237;a relevantes.
</p><p>Los indicadores fueron seleccionados para mostrar &#237;ndices de sequ&#237;a meteorol&#243;gico (d&#233;ficit pluviom&#233;trico), sequ&#237;a hidrol&#243;gico (d&#233;ficit hidrico) y sequ&#237;a agr&#237;cola (d&#233;ficit en la vegetaci&#243;n), y su efecto combinado.
</p>
</div>
<div class="rightcol tabbedentries" about="/maproom/Monitoring/" >
  <link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Meteorological_term" />
  <link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Hydrological_term" />
</div>

</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend>

</fieldset>
</div>
 </body>

 </html>
