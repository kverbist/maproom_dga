<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<meta xml:lang="" property="maproom:Entry_Id" content="Monitoring_Hydrological" />
<title>Caudales Observados</title>
<link rel="stylesheet" type="text/css" href="../../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="DischargeDGA.html?Set-Language=en" />
<link class="share" rel="canonical" href="DischargeDGA.html" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Hydrological_term"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
<link rel="term:icon" href="http://www.climatedatalibrary.cl/expert/SOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.hydrological/.Current/.Monthly/lon/lat/2/copy/Discharge_st_anomaly/6/-1/roll/pop//long_name/%28Standardized%20Discharge%20Anomaly%29def//name/%28st_discharge%29def/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//bbox%5B-80/-55/-67/-17%5Dpsdef//st_discharge/-3/3/plotrange/%28antialias%29true/psdef/%28fntsze%2920/psdef//T/last/plotvalue/Y/-56.0/-16.0/plotrange//plotaxislength/432/psdef/++//T/last/plotvalue+//plotborder+72+psdef//plotaxislength+432+psdef+.gif" />
<script type="text/javascript" src="../../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../../maproom/unesco.js"></script>
<style>
.dlimage.ver2 {
        width: 24%;
        float: right;
}
.dlimagei.ver3 {
        width: 99%;
}
.dlimage.ver4 {
        width: 33%;
}
.dlimgtsbox {
width: 99%;
display: inline-block
 }
</style>
</head>
<body  xml:lang="es">
<form name="pageform" id="pageform" class="carryup carry dlimg dlauximg dlimgts share"> 
<input class="carryLanguage carryup carry " name="Set-Language" type="hidden" />
<input class="carry dlimg dlimgloc admin maptable share" name="bbox" type="hidden" />
<input class="dlimg share" name="T" type="hidden" />
<input class="dlimg" name="plotaxislength" type="hidden" />
<input class="dlimg dlauximg dlimgts maptable  share" name="anal" type="hidden" />
<input class="share dlimgts dlimgloc dlimglocclick station" name="region" type="hidden" />
<input class="transformRegion dlimglocclick" name="clickpt" type="hidden" />
<input class="pickarea dlimgts admin " name="resolution" type="hidden" value="irids:SOURCES:Chile:DGA:hydrological:Current:Monthly:ds" />
<!-- list of layers form with names corresponding to different layers of the image so that they can be un/checked by default. I leave it commented out since as of now scatterlabel is not considered a layer
<input class="dlimg share" name="layers" value="Discharge" checked="checked" type="checkbox" />
<input class="dlimg share" name="layers" value="aprod" checked="checked" type="checkbox" />
<input class="dlimg share" name="layers" value="label" checked="unchecked" type="checkbox" />
<input class="dlimg share" name="layers" value="rivers_gaz" type="checkbox" />
<input class="dlimg share" name="layers" value="coasts_gaz" checked="checked" type="checkbox" />
<input class="dlimg share" name="layers" value="countries_gaz" type="checkbox" checked="checked" />
-->
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList"> 
                <legend>Maproom</legend> 
                      <a rev="section" class="navlink carryup" href="/maproom/Monitoring/">Monitoreo</a>
            </fieldset> 
            <fieldset class="navitem" id="chooseSection"> 
                <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Hydrological_term"><span property="term:label">Sequ&#237;a Hidrol&#243;gica</span></legend>
            </fieldset> 
            <fieldset class="navitem">
                <legend>Región</legend>
                <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsChile.json"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">Chile</option>
                <optgroup class="template" label="Región">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup>
                </select>
           </fieldset>
           <fieldset class="navitem">
	    <legend>Análisis</legend>
	     <span class="selectvalue"></span><select class="pageformcopy" name="anal"><option value="">Porcentaje</option><option value="Observed">Mediciones</option><option value="Anomaly">Anomal&#237;a</option></select>
           </fieldset>

          <!-- Menu generated by json to list stations according to their variable 'label'. The function labelgeoIdinteresects is directly defined in the link. When you have a more recent version of ingrid, you will be able to take it out. The function has 2 inputs: a resolution in the same format as resolution form/parameter and bbox (optional: by default the World) that constrains to list only labels that fall into bbox, also same format as bbox form/parameter. You may want to adjust default bbox so that it is consistent to the whole Maproom. The rest is fixed. The link must have the class admin (or you can name it otherwise but use same name in the forms declaration. -->
          <fieldset class="navitem">
          <legend>Selección de Estación</legend>	      
          <link class="admin" rel="iridl:hasJSON"
           href="http://www.climatedatalibrary.cl/expert/%28irids:SOURCES:Chile:DGA:hydrological:Current:Hourly:ds%29//resolution/parameter/geoobject/%28bb:-80:-55:-67:-17:bb%29//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
          <select class="pageformcopy" name="region">
          <optgroup class="template" label="Label">
          <option class="iridl:values region@value label"></option>
          </optgroup></select>
          </fieldset> 
</div>

<div class="ui-tabs">
    <ul class="ui-tabs-nav">
      <li><a href="#tabs-1" >Descripci&#243;n</a></li>
      <li><a href="#tabs-2" >M&#225;s informaci&#243;n</a></li>
      <li><a href="#tabs-3" >Fuente</a></li>
      <li><a href="#tabs-4" >Soporte</a></li>
      <li><a href="#tabs-5" >Instrucciones</a></li>
    </ul>

<fieldset class="regionwithinbbox dlimage" about="">
<a class="dlimgts" rel="iridl:hasTable" href="http://www.climatedatalibrary.cl/expert/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Monthly/.Monthly_discharge//long_name/%28Caudal%20Observado%29def/SOURCES/.Chile/.DGA/.hydrological/.Current/.Monthly/.Discharge_percentage/%28percent%29unitconvert//long_name/%28Porcentaje%20del%20Promedio%29def//units/%28%25%29def/SOURCES/.Chile/.DGA/.hydrological/.Current/.Monthly/.Discharge_st_anomaly//long_name/%28Anomalia%20Estandarizada%29def/3/array/astore/%7BT/last/dup/12.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Chile:DGA:hydrological:Current:Monthly:nombre%40RIO%20ACONCAGUA%20EN%20CHACABUQUITO:ds%29//region/parameter/geoobject/.nombre/.first/VALUE%7Dforall//name/%28st_precip%29def/T//long_name/%28Tiempo%29def/4/-3/roll/table:/4/:table/"></a>
<div style="float: left;">
<img class="dlimgloc" src="http://www.climatedatalibrary.cl/SOURCES/.WORLDBATH/.bath/X/Y/%28bb:-80%2C-56%2C-60%2C-16%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28pt:-80:-56:pt%29//region/parameter/geoobject/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/geometrypushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0.1/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif" />
</div>

<div style="float: left;">

<div class="valid" style="text-align: top;">
<a class="station" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/%28pt:-80:-56:pt%29//region/parameter/geoobject/info.json"></a>
<div class="template" style="color : black">
Estaci&#243;n DGA <b><span class="iridl:long_name"></span></b>
</div>
</div>

<div class="valid" style="text-align: top;">
<a class="station" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/nombre/%28irids:SOURCES:Chile:DGA:hydrological:Current:Hourly:nombre%40RIO%20ACONCAGUA%20EN%20CHACABUQUITO:ds%29//region/parameter/geoobject/.nombre/.first/VALUE/lon/lat/2/ds/info.json"></a>
<script type="application/json" property="iridl:hasPUREdirective">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"
}
}
}
</script>
<div><table class="valid template">
<tr style="color : black"><td class="name "></td><td align="right" class="value "></td></tr>
</table>
</div>

</div>
<div class="template"> <b>Observaciones para el mes actual: </b></div> 
<div class="valid" style="text-align: top;">
<a class="station" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Monthly/.Monthly_discharge/T/last/VALUE/nombre/%28irids:SOURCES:Chile:DGA:hydrological:Current:Monthly:nombre%40RIO%20ACONCAGUA%20EN%20CHACABUQUITO:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Caudal%20Observado%20%28m3/s%29:%20%29def/SOURCES/.Chile/.DGA/.hydrological/.Current/.Monthly/.Discharge_st_anomaly/T/last/VALUE/nombre/%28irids:SOURCES:Chile:DGA:hydrological:Current:Monthly:nombre%40RIO%20ACONCAGUA%20EN%20CHACABUQUITO:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Anomalia%20del%20Caudal%20Estandarizada%20%28-%29:%20%29def/SOURCES/.Chile/.DGA/.hydrological/.Current/.Monthly/.Discharge_percentage/%28percent%29unitconvert/T/last/VALUE/nombre/%28irids:SOURCES:Chile:DGA:hydrological:Current:Monthly:nombre%40RIO%20ACONCAGUA%20EN%20CHACABUQUITO:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Caudal%20-%20Porcentaje%20del%20Promedio%20%28%25%29:%20%29def/3/ds/info.json"></a>
<script type="application/json" property="iridl:hasPUREdirective">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"
}
}
}
</script>
<div><table class="valid template">
<tr style="color : black"><td class="name "></td><td align="right" class="value "></td></tr>
</table>
</div>

</div>

</div>

<br />
<div class="dlimgtsbox">
<img class="dlimgts regionwithinbbox" rel="iridl:hasFigureImage" src="http://www.climatedatalibrary.cl/expert/%28Percentage%29//anal/parameter/%28Anomaly%29eq/%7BSOURCES/.Chile/.DGA/.hydrological/.Current/.Monthly/.Discharge_st_anomaly/T/last/dup/18.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Chile:DGA:hydrological:Current:Monthly:nombre%40RIO%20ACONCAGUA%20EN%20CHACABUQUITO:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Anomalia%20del%20Caudal%20Estandarizada%29def//name/%28st_discharge%29def/pdsi_colorbars/DATA/AUTO/AUTO/RANGE/dup/T/fig-/colorbars2/-fig%7Dif//anal/get_parameter/%28Observed%29eq/%7BSOURCES/.Chile/.DGA/.hydrological/.Current/.Monthly/.Monthly_discharge/T/last/dup/18.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Chile:DGA:hydrological:Current:Monthly:nombre%40RIO%20ACONCAGUA%20EN%20CHACABUQUITO:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Caudal%20Observado%29def//name/%28st_discharge%29def/dup/T/fig-/colorbars2/-fig%7Dif//anal/get_parameter/%28Percentage%29eq/%7BSOURCES/.Chile/.DGA/.hydrological/.Current/.Monthly/.Discharge_percentage/%28percent%29unitconvert/100./min/T/last/dup/18.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Chile:DGA:hydrological:Current:Monthly:nombre%40RIO%20ACONCAGUA%20EN%20CHACABUQUITO:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Caudal%20-%20Porcentaje%20del%20Promedio%29def/name/%28st_discharge%29def/pdsi_colorbars/DATA/0./100./RANGE/dup/T/fig-/colorbars2/-fig%7Dif//plotborder/72/psdef//plotaxislength/432/psdef//plotborder/72/psdef//plotaxislength/432/psdef/+//plotborder+72+psdef//plotaxislength+432+psdef+.gif" />
</div>
<br />
<div class="dlimgtsbox">
<img class="dlimgts regionwithinbbox" rel="iridl:hasFigureImage" src="http://www.climatedatalibrary.cl/expert/expert/SOURCES/.Chile/.Analysis/.DGA/.Hydrological/.Current_complete/.Monthly_historical/.Monthly_discharge//fullname/%28Observed%20Discharge%29/def/nombre/%28irids:SOURCES:Chile:DGA:hydrological:Current:Monthly:nombre%40RIO%20ACONCAGUA%20EN%20CHACABUQUITO:ds%29//region/parameter/geoobject/.nombre/.first/VALUE/T/12/splitstreamgrid/dup/%5BT2%5Daverage/exch/%5BT2%5D0.05/0.5/0.95/0/replacebypercentile/a:/percentile/0.05/VALUE/percentile/removeGRID//fullname/%28M%C3%ADnimo%20Esperado%29/def/:a:/percentile/0.5/VALUE/percentile/removeGRID//fullname/%28Normal%29/def/:a:/percentile/0.95/VALUE/percentile/removeGRID//fullname/%28M%C3%A1ximo%20Esperado%29/def/:a/2/index/0.0/mul/dup/6/-1/roll/exch/6/-2/roll/5/index//long_name/%28Cuadal%20Promedio%20Mensual%29/def//units/%28m3/s%29/def/SOURCES/.Chile/.DGA/.hydrological/.Current/.Monthly/.Monthly_discharge/nombre/%28irids:SOURCES:Chile:DGA:hydrological:Current:Monthly:nombre%40RIO%20ACONCAGUA%20EN%20CHACABUQUITO:ds%29//region/parameter/geoobject/.nombre/.first/VALUE/T/last/dup/18/sub/exch/RANGE//fullname/%28Caudal%20Observado%29/def/DATA/0/AUTO/RANGE/6/5/roll/pop/7/-6/roll/6/array/astore/%7B%5BT%5DregridLinear%7Dforall/7/-1/roll/5/-4/roll/T/fig-/white/deltabars/grey/deltabars/solid/medium/green/line/blue/line/red/line/-fig//framelabel/%28Caudal%20Observado%20y%20Normal%29/psdef//plotborderbottom/40/psdef//antialias/true/def/nombre/last/plotvalue/+.gif" />
</div>
</fieldset>

<fieldset class="dlimage" id="content" about="">
<a class="maptable" rel="iridl:hasTable" href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Monthly/location/%28bb:-80:-55:-67:-17:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/%28Percentage%29//anal/parameter/%28Anomaly%29eq/%7B.Discharge_st_anomaly//long_name/%28Anomalia%20del%20Caudal%20Estandarizada%29def//units/%28-%29def%7Dif//anal/get_parameter/%28Observed%29eq/%7B.Monthly_discharge//long_name/%28Caudal%20Observado%29def//units/%28m3/s%29def%7Dif//anal/get_parameter/%28Percentage%29eq/%7B.Discharge_percentage//long_name/%28Caudal%20-%20Porcentaje%20del%20Promedio%29def//units/%28%25%29def%7Dif%5Bnombre%5DREORDER/mark/exch/T//long_name/%28Tiempo%29def/exch%5Bnombre%5Dtable:/mark/:table/" shape="rect"></a>
<link rel="iridl:hasFigure" href="http://www.climatedatalibrary.cl/expert/(Percentage)//anal/parameter/(Anomaly)/eq/{SOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.hydrological/.Current/.Monthly/lon/lat/2/copy/Discharge_st_anomaly/6/-1/roll/pop//long_name/(Anomalia del Caudal Estandarizada)/def//name/(st_discharge)/def/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//bbox[-80/-55/-67/-17]psdef//st_discharge/-3/3/plotrange/(antialias)/true/psdef/(fntsze)/20/psdef}if//anal/get_parameter/(Observed)/eq/{SOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.hydrological/.Current/.Monthly/lon/lat/2/copy/Monthly_discharge/6/-1/roll/pop//name/(discharge)/def//long_name/(Caudal Observado)/def/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef}if//anal/get_parameter/(Percentage)/eq/{SOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.hydrological/.Current/.Monthly/lon/lat/2/copy/Discharge_percentage/100./min/6/-1/roll/pop//long_name/(Caudal - Porcentaje del Promedio)/def//name/(st_discharge)/def/pdsi_colorbars/DATA/0./100./RANGE/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//bbox[-80/-55/-67/-17]psdef//st_discharge/-3/3/plotrange/(antialias)/true/psdef/(fntsze)/20/psdef}if//T/last/plotvalue/Y/-56.0/-16.0/plotrange//plotaxislength/432/psdef//plotborder/72/psdef/" />
<img class="dlimg" src="http://www.climatedatalibrary.cl/expert/(Percentage)//anal/parameter/(Anomaly)/eq/{SOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.hydrological/.Current/.Monthly/lon/lat/2/copy/Discharge_st_anomaly/6/-1/roll/pop//long_name/(Anomalia del Caudal Estandarizada)/def//name/(st_discharge)/def/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//bbox[-80/-55/-67/-17]psdef//st_discharge/-3/3/plotrange/(antialias)/true/psdef/(fntsze)/20/psdef}if//anal/get_parameter/(Observed)/eq/{SOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.hydrological/.Current/.Monthly/lon/lat/2/copy/Monthly_discharge/6/-1/roll/pop//name/(discharge)/def//long_name/(Caudal Observado)/def/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef}if//anal/get_parameter/(Percentage)/eq/{SOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.hydrological/.Current/.Monthly/lon/lat/2/copy/Discharge_percentage/100./min/6/-1/roll/pop//long_name/(Caudal - Porcentaje del Promedio)/def//name/(st_discharge)/def/pdsi_colorbars/DATA/0./100./RANGE/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//bbox[-80/-55/-67/-17]psdef//st_discharge/-3/3/plotrange/(antialias)/true/psdef/(fntsze)/20/psdef}if//T/last/plotvalue/Y/-56.0/-16.0/plotrange//plotaxislength/432/psdef//plotborder/72/psdef/+.gif"  border="0" alt="image" /><br />
<img class="dlauximg" src="http://www.climatedatalibrary.cl/expert/(Percentage)//anal/parameter/(Anomaly)/eq/{SOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.hydrological/.Current/.Monthly/lon/lat/2/copy/Discharge_st_anomaly/6/-1/roll/pop//long_name/(Anomalia del Caudal Estandarizada)/def//name/(st_discharge)/def/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//bbox[-80/-55/-67/-17]psdef//st_discharge/-3/3/plotrange/(antialias)/true/psdef/(fntsze)/20/psdef}if//anal/get_parameter/(Observed)/eq/{SOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.hydrological/.Current/.Monthly/lon/lat/2/copy/Monthly_discharge/6/-1/roll/pop//name/(discharge)/def//long_name/(Caudal Observado)/def/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef}if//anal/get_parameter/(Percentage)/eq/{SOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.hydrological/.Current/.Monthly/lon/lat/2/copy/Discharge_percentage/100./min/6/-1/roll/pop//long_name/(Caudal - Porcentaje del Promedio)/def//name/(st_discharge)/def/pdsi_colorbars/DATA/0./100./RANGE/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//bbox[-80/-55/-67/-17]psdef//st_discharge/-3/3/plotrange/(antialias)/true/psdef/(fntsze)/20/psdef}if//T/last/plotvalue/Y/-56.0/-16.0/plotrange//plotaxislength/432/psdef//plotborder/72/psdef/+.auxfig/+.gif" />
</fieldset>
 <div id="tabs-1" class="ui-tabs-panel" about="">
<h3 align="center"  property="term:title" >Caudales Observados</h3>
<p align="left" property="term:description">Este mapa muestra el caudal en las cuencas principales de Chile. </p>
<p align="left">Selecciona la variable de inter&#233;s en el men&#250;>an&#225;lisis: mediciones o anomal&#237;a. En el men&#250;>regi&#243;n se puede seleccionar la regi&#243;n de inter&#233;s.</p>

<p align="left"><b>Mediciones:</b>
Este mapa muestra los caudales observados en las principales cuencas de Chile. Las mediciones est&#225;n en m3/s. Los caudales est&#225;n disponibles por cada mes.</p>
<p align="left"><b>Porcentaje:</b>
Este mapa muestra el caudal como porcentaje del caudal normalmente esperado en cada mes. El porcentaje indica si hay un d&#233;ficit o super&#225;vit comparada con una situaci&#243;n normal.</p>
<p align="left"><b>Anomal&#237;a estandarizada:</b>
El mapa muestra los caudales observados como anomal&#237;a estandarizada. La anomal&#237;a estandarizada es la diferencia entre el caudal observado en un mes espec&#237;fico y el caudal esperado normalmente en el mismo mes, y permite identificar condiciones de d&#233;ficit y de super&#225;vit con respecto a lo normal (Tabla 1).</p>
<p align="left"><h6> Tabla 1: Interpretaci&#243;n de la Anomal&#237;a Estandarizada</h6></p>
<p align="left"><img src="Escala_anomalias_caudal_esp.jpg" alt="Leyenda de la Anomal&#237;a Estandarizada"> </img></p>
<p align="left">Los datos provienen de las estaciones fluviom&#233;tricas de la Direcci&#243;n General de Aguas de Chile (DGA).</p>
<p align="left">  <img src="../../icons/Logo_dga" alt="Logo DGA"></img></p>
</div>

<div id="tabs-2"  class="ui-tabs-panel">
<h3  align="center">&#191;C&#243;mo se calcula el porcentaje?</h3>
<p>
El porcentaje se calcula dividiendo el caudal observada en un mes con el caudal normalmente esperada en este mes (promedio) multiplicado con cien. </p>
<h3  align="center">&#191;C&#243;mo se calcula la anomal&#237;a?</h3>
<p>La anomal&#237;a es el caudal mensual observado en una estaci&#243;n espec&#237;fica menos el promedio del caudal mensual (calculado usando datos hist&#243;ricos de la estaci&#243;n espec&#237;fica) dividido por la desviaci&#243;n est&#225;ndar.
</p>
</div>
<div id="tabs-3" class="ui-tabs-panel">
<h3  align="center">Fuente de los Datos</h3>
<p><a href="http://climatedatalibrary.cl/SOURCES/.Chile/.DGA/.hydrological/.regionIV/.station/.daily/"
>Caudales observados</a>, entregado por la Direcci&#243;n General de Aguas, <a href="http://www.dga.cl/">(DGA)</a></p>
</div>

<div id="tabs-4"  class="ui-tabs-panel">
<h3  align="center">Soporte</h3>
<p>
Sus consultas a: <a href="mailto:mwar_lac@unesco.org?subject=Precipitacion Observada Chile">mwar_lac@unesco.org</a>
 </p>
</div>
<div id="tabs-5"  class="ui-tabs-panel">
<h3  align="center">Instrucciones</h3>
<div class="buttonInstructions"></div> 
</div>
</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend></fieldset>
</div>
 </body>
 </html>
