<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<meta xml:lang="" property="maproom:Entry_Id" content="Monitoring_Hydrological" />
<title>Ruta de Nieve Observado</title>
<link rel="stylesheet" type="text/css" href="../../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="RutaNieveDGA.html?Set-Language=en" />
<link class="share" rel="canonical" href="RutaNieveDGA.html" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Hydrological_term"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
<link rel="term:icon" href="http://www.climatedatalibrary.cl/expert/%28Percentage%29//anal/parameter/%28Anomaly%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.snow/.Monthly/lon/lat/2/copy/snow_depth_st_anomaly/6/-1/roll/pop//long_name/%28Anomalia%20de%20la%20Altura%20de%20Nieve%20Estandardizada%29/def/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//bbox%5B-80/-55/-67/-17%5Dpsdef//st_discharge/-3/3/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif//anal/get_parameter/%28Observed%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.snow/.Monthly/lon/lat/2/copy/snow_depth/6/-1/roll/pop//long_name/%28Nivel%20de%20Nieve%20Observado%29/def/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef%7Dif//anal/get_parameter/%28Percentage%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.snow/.Monthly/lon/lat/2/copy/snow_depth_percentage/100./min/6/-1/roll/pop//long_name/%28Altura%20de%20Nieve%20-%20Porcentaje%20de%20lo%20Normal%29/def/pdsi_colorbars/DATA/0./100./RANGE/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//bbox%5B-80/-55/-67/-17%5Dpsdef/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif//T/last/plotvalue/Y/-56.0/-16.0/plotrange//plotaxislength/432/psdef//plotborder/72/psdef+//plotborder+0+psdef//plotaxislength+589+psdef//fntsze+20+psdef//antialias+true+psdef//bbox+[%20-80%20-55%20-67%20-17]+psdef//color_smoothing+1+psdef+.gif" />
<script type="text/javascript" src="../../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../../maproom/unesco.js"></script>
<style>
.dlimage.ver2 {
        width: 24%;
        float: right;
}
.dlimagei.ver3 {
        width: 99%;
}
.dlimage.ver4 {
        width: 33%;
}
.dlimgtsbox {
width: 99%;
display: inline-block
 }
</style>
</head>
<body  xml:lang="es">
<form name="pageform" id="pageform" class="carryup carry dlimg dlauximg dlimgts share"> 
<input class="carryLanguage carryup carry " name="Set-Language" type="hidden" />
<input class="carry dlimg dlimgloc admin maptable share" name="bbox" type="hidden" />
<input class="dlimg share" name="T" type="hidden" />
<input class="dlimg" name="plotaxislength" type="hidden" />
<input class="dlimg dlauximg dlimgts maptable  share" name="anal" type="hidden" />
<input class="share dlimgts dlimgloc dlimglocclick station" name="region" type="hidden" />
<input class="transformRegion dlimglocclick" name="clickpt" type="hidden" />
<input class="pickarea dlimgts admin " name="resolution" type="hidden" value="irids:SOURCES:Chile:DGA:snow:Monthly:ds" />
<!-- list of layers form with names corresponding to different layers of the image so that they can be un/checked by default. I leave it commented out since as of now scatterlabel is not considered a layer
<input class="dlimg share" name="layers" value="Discharge" checked="checked" type="checkbox" />
<input class="dlimg share" name="layers" value="aprod" checked="checked" type="checkbox" />
<input class="dlimg share" name="layers" value="label" checked="unchecked" type="checkbox" />
<input class="dlimg share" name="layers" value="rivers_gaz" type="checkbox" />
<input class="dlimg share" name="layers" value="coasts_gaz" checked="checked" type="checkbox" />
<input class="dlimg share" name="layers" value="countries_gaz" type="checkbox" checked="checked" />
-->
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList"> 
                <legend>Maproom</legend> 
                      <a rev="section" class="navlink carryup" href="/maproom/Monitoring/">Monitoreo</a>
            </fieldset> 
            <fieldset class="navitem" id="chooseSection"> 
                <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Hydrological_term"><span property="term:label">Sequ&#237;a Hidrol&#243;gica</span></legend>
            </fieldset> 
            <fieldset class="navitem">
                <legend>Región</legend>
                <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsChile.json"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">Chile</option>
                <optgroup class="template" label="Región">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup>
                </select>
           </fieldset>
           <fieldset class="navitem">
	    <legend>Análisis</legend>
	     <span class="selectvalue"></span><select class="pageformcopy" name="anal"><option value="">Porcentaje</option><option value="Observed">Mediciones</option><option value="Anomaly">Anomal&#237;a</option></select>
           </fieldset>

          <!-- Menu generated by json to list stations according to their variable 'label'. The function labelgeoIdinteresects is directly defined in the link. When you have a more recent version of ingrid, you will be able to take it out. The function has 2 inputs: a resolution in the same format as resolution form/parameter and bbox (optional: by default the World) that constrains to list only labels that fall into bbox, also same format as bbox form/parameter. You may want to adjust default bbox so that it is consistent to the whole Maproom. The rest is fixed. The link must have the class admin (or you can name it otherwise but use same name in the forms declaration. -->
         <fieldset class="navitem">
            
            <legend>Selección de Estación</legend>	      
            
            <link class="admin" rel="iridl:hasJSON"
                  href="http://www.climatedatalibrary.cl/expert/(irids%3ASOURCES%3AChile%3ADGA%3Asnow%3AMonthly%3Ads)//resolution/parameter/geoobject/(bb%3A-80%3A-55%3A-67%3A-17%3Abb)//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
            <select class="pageformcopy" name="region">
               <optgroup class="template" label="Label">
                  <option class="iridl:values region@value label"></option>
               </optgroup></select>
            
         </fieldset> 
</div>

<div class="ui-tabs">
    <ul class="ui-tabs-nav">
      <li><a href="#tabs-1" >Descripci&#243;n</a></li>
      <li><a href="#tabs-2" >M&#225;s informaci&#243;n</a></li>
      <li><a href="#tabs-3" >Fuente</a></li>
      <li><a href="#tabs-4" >Soporte</a></li>
      <li><a href="#tabs-5" >Instrucciones</a></li>
    </ul>

         <fieldset class="regionwithinbbox dlimage" about="">
            <a class="dlimgts" rel="iridl:hasTable"
               href="http://www.climatedatalibrary.cl/expert/expert/SOURCES/.Chile/.DGA/.snow/.Monthly/.snow_depth//long_name/%28Observed%20Snow%20Depth%29def/SOURCES/.Chile/.DGA/.snow/.Monthly/.snow_depth_percentage/%28percent%29unitconvert//long_name/%28Snow%20Depth%20-%20Percentage%20of%20Normal%29def/SOURCES/.Chile/.DGA/.snow/.Monthly/.snow_depth_st_anomaly//long_name/%28Standardized%20Snow%20Depth%20Anomaly%29def/3/array/astore/%7BT/last/dup/120.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Chile:DGA:snow:Monthly:nombre%40PUCLARO:ds%29//region/parameter/geoobject/.nombre/.first/VALUE%7Dforall//name/%28st_precip%29def/T/4/-3/roll/table:/4/:table/"
               shape="rect"></a>
            
            <div style="float: left;">
               <img class="dlimgloc"
                    src="http://www.climatedatalibrary.cl/SOURCES/.WORLDBATH/.bath/X/Y/%28bb:-80%2C-56%2C-60%2C-16%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28pt:-80:-56:pt%29//region/parameter/geoobject/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/geometrypushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0.1/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif" />
               
            </div>
            
            
            <div style="float: left;">
               
               
               <div class="valid" style="text-align: top;">
                  <a class="station" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/expert/%28pt:-80:-56:pt%29//region/parameter/geoobject/info.json"
                     shape="rect"></a>
                  
                  <div class="template" style="color : black">
                     Estación Ruta de Nieve DGA <b><span class="iridl:long_name"></span></b>
                     
                  </div>
                  
               </div>
               
               
               <div class="valid" style="text-align: top;">
                  <a class="station" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.snow/.Monthly/nombre/%28irids:SOURCES:Chile:DGA:snow:Monthly:nombre%40PORTILLO:ds%29//region/parameter/geoobject/.nombre/.first/VALUE/lon/lat/2/ds/info.json"
                     shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"
}
}
}
</script>
                  <div>
                     <table class="valid template">
                        <tr style="color : black">
                           <td class="name " rowspan="1" colspan="1"></td>
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                        </tr>
                     </table>
                     
                  </div>
                  
                  
               </div>
               
               <div class="template"> <b>Observaciones de la &#250;ltima medici&#243;n: </b></div> 
               
               <div class="valid" style="text-align: top;">
                  <a class="station" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.snow/.Monthly/.snow_depth_st_anomaly/T/last/VALUE/nombre/(irids%3ASOURCES%3AChile%3ADGA%3Asnow%3AMonthly%3Anombre%40PORTILLO%3Ads)//region/parameter/geoobject/.nombre/.first/VALUE//long_name/(Anomalia%20de%20la%20Altura%20de%20Nieve%20Estandardizada%20(-)%3A%20)/def/SOURCES/.Chile/.DGA/.snow/.Monthly/.snow_depth_percentage/(percent)/unitconvert/T/last/VALUE/nombre/(irids%3ASOURCES%3AChile%3ADGA%3Asnow%3AMonthly%3Anombre%40PORTILLO%3Ads)//region/parameter/geoobject/.nombre/.first/VALUE//long_name/(Altura%20de%20Nieve%20-%20Porcentaje%20de%20lo%20Normal%20(%25)%3A%20)/def/SOURCES/.Chile/.DGA/.snow/.Monthly/.snow_depth/T/last/VALUE/nombre/(irids%3ASOURCES%3AChile%3ADGA%3Asnow%3AMonthly%3Anombre%40PORTILLO%3Ads)//region/parameter/geoobject/.nombre/.first/VALUE//long_name/(Nivel%20de%20Nieve%20Observado%20(cm)%3A%20)/def/3/ds/info.json"
                     shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"
}
}
}
</script>
                  <div>
                     <table class="valid template">
                        <tr style="color : black">
                           <td class="name " rowspan="1" colspan="1"></td>
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                        </tr>
                     </table>
                     
                  </div>
                  
                  
               </div>
               
               
            </div>
            
            <br clear="none" />
            
            <div class="dlimgtsbox">
               <img class="dlimgts regionwithinbbox" rel="iridl:hasFigureImage"
                    src="http://www.climatedatalibrary.cl/expert/%28Percentage%29//anal/parameter/%28Anomaly%29/eq/%7BSOURCES/.Chile/.DGA/.snow/.Monthly/.snow_depth_st_anomaly/T/last/dup/60.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Chile:DGA:snow:Monthly:nombre%40PORTILLO:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Anomalia%20de%20la%20Altura%20de%20Nieve%20Estandardizada%29/def//name/%28st_discharge%29/def/pdsi_colorbars/DATA/AUTO/AUTO/RANGE/dup/T/fig-/colorbars2/-fig%7Dif//anal/get_parameter/%28Observed%29/eq/%7BSOURCES/.Chile/.DGA/.snow/.Monthly/.snow_depth/T/last/dup/60.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Chile:DGA:snow:Monthly:nombre%40PORTILLO:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Nivel%20de%20Nieve%20Observado%29/def//name/%28st_discharge%29/def/dup/T/fig-/colorbars2/-fig%7Dif//anal/get_parameter/%28Percentage%29/eq/%7BSOURCES/.Chile/.DGA/.snow/.Monthly/.snow_depth_percentage/%28percent%29/unitconvert/T/last/dup/60.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Chile:DGA:snow:Monthly:nombre%40PORTILLO:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Altura%20de%20Nieve%20-%20Porcentaje%20de%20lo%20Normal%29/def/name/%28st_discharge%29/def/pdsi_colorbars/DATA/0.0/100.0/RANGE/dup/T/fig-/colorbars2/-fig%7Dif//plotborder/72/psdef//plotaxislength/432/psdef//plotborder/72/psdef//plotaxislength/432/psdef//plotborder/72/psdef//plotaxislength/432/psdef/nombre/last/plotvalue+.gif" />
               
            </div>
            <br clear="none" />
            
            <div class="dlimgtsbox">
               <img class="dlimgts regionwithinbbox" rel="iridl:hasFigureImage"
                    src="http://www.climatedatalibrary.cl/expert/expert/SOURCES/.Chile/.DGA/.snow/.Monthly/.snow_depth//fullname/%28Altura%20de%20Nieve%20Observedo%29/def/nombre/%28irids:SOURCES:Chile:DGA:snow:Monthly:nombre%40PORTILLO:ds%29//region/parameter/geoobject/.nombre/.first/VALUE/T/12/splitstreamgrid/dup/%5BT2%5Daverage/exch/%5BT2%5D0.05/0.5/0.95/0/replacebypercentile/a:/percentile/0.05/VALUE/percentile/removeGRID//fullname/%28Altura%20M%C3%ADnima%29/def/:a:/percentile/0.5/VALUE/percentile/removeGRID//fullname/%28Normal%29/def/:a:/percentile/0.95/VALUE/percentile/removeGRID//fullname/%28Altura%20M%C3%A1xima%29/def/:a/2/index/0.0/mul/dup/6/-1/roll/exch/6/-2/roll/5/index//long_name/%28Altura%20de%20Nieve%29/def//units/%28cm%29/def/SOURCES/.Chile/.DGA/.snow/.Monthly/.snow_depth/nombre/%28irids:SOURCES:Chile:DGA:snow:Monthly:nombre%40PORTILLO:ds%29//region/parameter/geoobject/.nombre/.first/VALUE/T/last/dup/18/sub/exch/RANGE//fullname/%28Altura%20de%20Nieve%20Observado%29/def/DATA/0/AUTO/RANGE/6/5/roll/pop/7/-6/roll/6/array/astore/%7B%5BT%5DregridLinear%7Dforall/7/-1/roll/5/-4/roll/T/fig-/white/deltabars/grey/deltabars/solid/medium/green/line/blue/line/red/line/-fig//framelabel/%28Altura%20de%20Nieve%20Observado%20y%20Normal%29/psdef//plotborderbottom/40/psdef//antialias/true/def/nombre/last/plotvalue/+.gif" />
               
            </div>
            
         </fieldset>
         
         
         <fieldset class="dlimage" id="content" about="">
            <a class="maptable" rel="iridl:hasTable"
               href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.snow/.Monthly/location/%28bb:-80:-55:-67:-17:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/%28Percentage%29//anal/parameter/%28Anomaly%29eq/%7B.snow_depth_st_anomaly%7Dif//anal/get_parameter/%28Observed%29eq/%7B.snow_depth%7Dif//anal/get_parameter/%28Percentage%29eq/%7B.snow_depth_percentage%7Dif%5Bnombre%5DREORDER/mark/exch/T/exch%5Bnombre%5Dtable:/mark/:table/"
               shape="rect"></a>
            
            <link rel="iridl:hasFigure"
                  href="http://www.climatedatalibrary.cl/expert/(Percentage)//anal/parameter/(Anomaly)/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.snow/.Monthly/lon/lat/2/copy/snow_depth_st_anomaly/6/-1/roll/pop//long_name/(Anomalia%20de%20la%20Altura%20de%20Nieve%20Estandardizada)/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//bbox%5B-80/-55/-67/-17%5Dpsdef//st_discharge/-3/3/plotrange/(antialias)/true/psdef/(fntsze)/20/psdef%7Dif//anal/get_parameter/(Observed)/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.snow/.Monthly/lon/lat/2/copy/snow_depth/6/-1/roll/pop//long_name/(Nivel%20de%20Nieve%20Observado)/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef%7Dif//anal/get_parameter/(Percentage)/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.snow/.Monthly/lon/lat/2/copy/snow_depth_percentage/100./min/6/-1/roll/pop//long_name/(Altura%20de%20Nieve%20-%20Porcentaje%20de%20lo%20Normal)/def/pdsi_colorbars/DATA/0./100./RANGE/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//bbox%5B-80/-55/-67/-17%5Dpsdef/(antialias)/true/psdef/(fntsze)/20/psdef%7Dif//T/last/plotvalue/Y/-56.0/-16.0/plotrange//plotaxislength/432/psdef//plotborder/72/psdef/#expert" />
            <img class="dlimg"
                 src="http://www.climatedatalibrary.cl/expert/%28Percentage%29//anal/parameter/%28Anomaly%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.snow/.Monthly/lon/lat/2/copy/snow_depth_st_anomaly/6/-1/roll/pop//long_name/%28Anomalia%20de%20la%20Altura%20de%20Nieve%20Estandardizada%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//bbox%5B-80/-55/-67/-17%5Dpsdef//st_discharge/-3/3/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif//anal/get_parameter/%28Observed%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.snow/.Monthly/lon/lat/2/copy/snow_depth/6/-1/roll/pop//long_name/%28Nivel%20de%20Nieve%20Observado%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef%7Dif//anal/get_parameter/%28Percentage%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.snow/.Monthly/lon/lat/2/copy/snow_depth_percentage/100./min/6/-1/roll/pop//long_name/%28Altura%20de%20Nieve%20-%20Porcentaje%20de%20lo%20Normal%29/def/pdsi_colorbars/DATA/0./100./RANGE/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//bbox%5B-80/-55/-67/-17%5Dpsdef/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif//T/last/plotvalue/Y/-56.0/-16.0/plotrange//plotaxislength/432/psdef//plotborder/72/psdef/+.gif"
                 border="0"
                 alt="image" /><br clear="none" />
            <img class="dlauximg"
                 src="http://www.climatedatalibrary.cl/expert/%28Percentage%29//anal/parameter/%28Anomaly%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.snow/.Monthly/lon/lat/2/copy/snow_depth_st_anomaly/6/-1/roll/pop//long_name/%28Anomalia%20de%20la%20Altura%20de%20Nieve%20Estandardizada%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//bbox%5B-80/-55/-67/-17%5Dpsdef//st_discharge/-3/3/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif//anal/get_parameter/%28Observed%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.snow/.Monthly/lon/lat/2/copy/snow_depth/6/-1/roll/pop//long_name/%28Nivel%20de%20Nieve%20Observado%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef%7Dif//anal/get_parameter/%28Percentage%29/eq/%7BSOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.snow/.Monthly/lon/lat/2/copy/snow_depth_percentage/100./min/6/-1/roll/pop//long_name/%28Altura%20de%20Nieve%20-%20Porcentaje%20de%20lo%20Normal%29/def/pdsi_colorbars/DATA/0./100./RANGE/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//bbox%5B-80/-55/-67/-17%5Dpsdef/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif//T/last/plotvalue/Y/-56.0/-16.0/plotrange//plotaxislength/432/psdef//plotborder/72/psdef/+.auxfig/+.gif" />
            
         </fieldset>
         
         <div id="tabs-1" class="ui-tabs-panel" about="">
            
            <h3 align="center" property="term:title">Niveles de nieve</h3>
            
            <p align="left" property="term:description">Este mapa muestra los niveles de nieve en la cordillera de Chile. </p>
            
            <p align="left">Selecciona la variable de interés en el menú&gt;análisis: mediciones o anomalía. En el menú&gt;región se puede seleccionar la región
               de interés.
            </p>
            
            
            <p align="left"><b>Mediciones:</b>
               Este mapa muestra los niveles de nieve observados en la cordillera de Chile. Las mediciones están en cm. Los niveles
               están disponibles irregularmente a nivel mensual.
            </p>
            
            <p align="left"><b>Porcentaje:</b>
               Este mapa muestra el nivel de nieve como porcentaje del nivel normalmente esperado en cada mes. El porcentaje indica si hay un déficit
               o superávit comparada con una situación normal.
            </p>
            
            <p align="left"><b>Anomalía estandarizada:</b>
               El mapa muestra los niveles de nieve observados como anomalía estandarizada. La anomalía estandarizada es la diferencia entre el nivel
               observado en un mes específico y el nivel esperado normalmente en el mismo mes, y permite identificar condiciones de déficit
               y de superávit con respecto a lo normal (Tabla 1).
            </p>
            
            <p align="left">
               <h6> Tabla 1: Interpretación de la Anomalía Estandarizada</h6>
            </p>
            
            <p align="left"><img src="Escala_anomalias_caudal_esp.jpg"
                    alt="Leyenda de la Anomalía Estandarizada"> </img></p>
            
            <p align="left">Los datos provienen de las estaciones de la ruta de Nieve de la Dirección General de Aguas de Chile (DGA).</p>
            
            <p align="left">  <img src="../../icons/Logo_dga" alt="Logo DGA" /></p>
            
         </div>
         
         
         <div id="tabs-2" class="ui-tabs-panel">
            
            <h3 align="center">¿Cómo se calcula el porcentaje?</h3>
            
            <p>
               El porcentaje se calcula dividiendo el nivel de nieve observado en un mes con el nivel normalmente esperado en este mes (promedio)
               multiplicado con cien. 
            </p>
            
            <h3 align="center">¿Cómo se calcula la anomalía?</h3>
            
            <p>La anomalía es el nivel de nieve mensual observado en una estación específica menos el promedio del nivel mensual (calculado usando
               datos históricos de la estación específica) dividido por la desviación estándar.
               
            </p>
            
         </div>
         
         <div id="tabs-3" class="ui-tabs-panel">
            
            <h3 align="center">Fuente de los Datos</h3>
            
            <p><a href="http://www.climatedatalibrary.cl/SOURCES/.Chile/.DGA/.snow/.Monthly/.snow_depth/"
                  shape="rect">Niveles de nieve observados</a>, entregado por la Dirección General de Aguas, <a href="http://www.dga.cl/" shape="rect">(DGA)</a></p>
            
         </div>
         
         
         <div id="tabs-4" class="ui-tabs-panel">
            
            <h3 align="center">Soporte</h3>
            
            <p>
               Sus consultas a: <a href="mailto:mwar-lac@unesco.org?subject=Nivel de nieve Observado Chile"
                  shape="rect">mwar_lac@unesco.org</a>
               
            </p>
            
         </div>
         
         <div id="tabs-5" class="ui-tabs-panel">
            
            <h3 align="center">Instrucciones</h3>
            
            <div class="buttonInstructions"></div> 
            
         </div>
         
      </div>
      
      <div class="optionsBar">
         
         <fieldset class="navitem" id="share">
            <legend>Compartir</legend>
         </fieldset>
         
      </div>
 </body>
 </html>
