<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
      >
<head>
     <title>Alertas de Niveles de Rios</title>
      <link rel="stylesheet" type="text/css" href="../../unesco.css" />
      <link class="altLanguage" rel="alternate" hreflang="en"
            href="AlertasDGA.html?Set-Language=en" />
      <link class="share" rel="canonical" href="AlertasDGA.html" />
      <link rel="term:isDescribedBy"
            href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Alerts_Hydrological_term" />
      <link rel="term:isDescribedBy"
            href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
      <link rel="term:icon"
            href="http://www.climatedatalibrary.cl/expert/SOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.hydrological/.Current/a:/.Hourly/a:/.lon/:a:/.lat/2/copy/:a:/.water_level/:a%5BT/nombre%5DREORDER/CopyStream/0.0/100.0/masknotrange/:a:/.Alert_levels/:a/AZUL/0.0/mul//NO_ALERT/add_variable/%7BNO_ALERT/AZUL/AMARILLA/ROJA%7Dgrouptogrid/-999.0/maskle/sub/0.0/masklt/100.0/div/-1/mul/1/add%5BM%5Ddominant_class//long_name/%28Water%20Level%20Alert%29def/T/last/dup/7/sub/exch/RANGE/startcolormap/transparent/green/green/blue/yellow/red/red/endcolormap/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//bbox%5B-80/-55/-67/-17%5Dpsdef//antialias/true/psdef/T/last/plotvalue/X/-80.04166/-64.95834/plotrange/Y/-56/-16/plotrange//plotaxislength/432/psdef//XOVY/null/psdef//plotborder/72/psdef//plotbordertop/20/psdef//plotborderbottom/60/psdef//framelabel/%28Hora%20y%20fecha:%20%25=%5BT%5D%29psdef+//plotborder+0+psdef//plotaxislength+589+psdef//antialias+true+psdef//bbox+[%20-80%20-55%20-67%20-17]+psdef//color_smoothing+1+psdef//framelabel+%28Hora%20y%20fecha:%20%25=%5BT%5D%29+psdef+.gif" /><script type="text/javascript" src="../../../uicore/uicore.js" xml:space="preserve"></script><script type="text/javascript" src="../../../maproom/unesco.js" xml:space="preserve"></script></head>
   <body xml:lang="es">
      
      <form name="pageform" id="pageform" method="get"
            enctype="application/x-www-form-urlencoded">
         <input class="carryup carry " name="Set-Language" type="hidden" />
         <input class="carry dlimg dlimgloc admin share" name="bbox" type="hidden" />
         <input class="dlimg share" name="T" type="hidden" />
         <input class="dlimg" name="plotaxislength" type="hidden" />
         <input class="dlimgts dlimgloc share" name="region" type="hidden" />
         <input class="pickarea admin" name="resolution" type="hidden"
                data-default="irids:SOURCES:Chile:DGA:hydrological:Current:Hourly:ds" />
         
         
      </form>
      
      <div class="controlBar">
         
         <fieldset class="navitem" id="toSectionList"> 
            
            <legend>Maproom</legend> 
            <a rev="section" class="navlink carryup" href="/maproom/Alerts/" shape="rect">Alertas</a>
            
         </fieldset> 
         
         <fieldset class="navitem" id="chooseSection"> 
            
            <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Alerts_Hydrological_term"><span property="term:label">Alertas Hidrológicas</span></legend>
            
         </fieldset> 
         
         <fieldset class="navitem">
            
            <legend>Región</legend>
            <a class="carryLanguage" rel="iridl:hasJSON"
               href="/maproom/globalregionsChile.json"
               shape="rect"></a>
            <select class="RegionMenu" name="bbox">
               <option value="">Chile</option>
               <optgroup class="template" label="Región">
                  <option class="irigaz:hasPart irigaz:id@value term:label"></option>
               </optgroup></select>
            
         </fieldset>
         
         <!--            <fieldset class="navitem">
		<legend>Variable</legend>
	<span class="selectvalue"></span><select class="pageformcopy" name="anal"><option value="">Anomalia</option><option value="Observed">Observado</option></select></fieldset>
-->
         
         
         <fieldset class="navitem">
            <legend>Selección de Estación</legend>	
            
            <link class="admin" rel="iridl:hasJSON"
                  href="http://www.climatedatalibrary.cl/expert/%28irids:SOURCES:Chile:DGA:hydrological:Current:Hourly:ds%29//resolution/parameter/geoobject/%28bb:-80:-55:-67:-17:bb%29//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
            <select class="pageformcopy" name="region">
               <optgroup class="template" label="Label">
                  <option class="iridl:values region@value label"></option>
               </optgroup></select>
            
         </fieldset>
         
         
      </div>
      
      <div class="ui-tabs">
         
         <ul class="ui-tabs-nav">
            <li><a href="#tabs-1" shape="rect">Descripción</a></li>
            <li><a href="#tabs-2" shape="rect">Más información</a></li>
            <li><a href="#tabs-3" shape="rect">Fuente</a></li>
            <li><a href="#tabs-4" shape="rect">Soporte</a></li>
         </ul>
         
         
         <fieldset class="regionwithinbbox dlimage" about="">
            
            
            <div style="float: left;">
               <img class="dlimgloc"
                    src="http://www.climatedatalibrary.cl/SOURCES/.WORLDBATH/.bath/X/Y/%28bb:-80%2C-56%2C-60%2C-16%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28irids%3ASOURCES%3AChile%3ADGA%3Ahydrological%3ACurrent%3AHourly%3Anombre%40RIO%20SALADO%20EN%20SIFON%20AYQUINA%3Ads%29//region/parameter/geoobject/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/geometrypushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0.1/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif" />
               
            </div>
            
            <div style="float: left;">
               
               <div class="valid" style="text-align: top;">
                  <a class="dlimgloc" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/expert/%28irids%3ASOURCES%3AChile%3ADGA%3Ahydrological%3ACurrent%3AHourly%3Anombre%40RIO%20SALADO%20EN%20SIFON%20AYQUINA%3Ads%29//region/parameter/geoobject/info.json"
                     shape="rect"></a>
                  
                  <div class="template" style="color : black">Estación DGA <b><span class="iridl:long_name"></span></b>
                     
                  </div>
                  
               </div>

               <div class="valid" style="text-align: top;">
                  <a class="dlimgloc" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/nombre/%28irids%3ASOURCES%3AChile%3ADGA%3Ahydrological%3ACurrent%3AHourly%3Anombre%40RIO%20SALADO%20EN%20SIFON%20AYQUINA%3Ads%29//region/parameter/geoobject/.nombre/.first/VALUE/lon/lat/2/ds/info.json"
                     shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"
}
}
}
</script>
                  <div>
                     <table class="valid template">
                        <tr style="color : black">
                           <td class="name " rowspan="1" colspan="1"></td>
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                        </tr>
                     </table>
                     
                  </div>
                  
               </div>
               
         <div class="valid" style="text-align: top;">
                  <a class="dlimgloc" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/a%3A/.Hourly/.water_level/%5BT/nombre%5DREORDER/CopyStream/0.0/100.0/masknotrange/%3Aa%3A/.Alert_levels/%3Aa/AZUL/0.0/mul//SIN_ALERTA/add_variable/%7BSIN_ALERTA/AZUL/AMARILLA/ROJA%7Dgrouptogrid/-999.0/maskle/sub/0.0/masklt/100.0/div/-1/mul/1/add/%5BM%5Ddominant_class//long_name/(Alerta%20sobre%20el%20Nivel%20del%20Rio)/def/T/last/VALUE/nombre/(irids%3ASOURCES%3AChile%3ADGA%3Ahydrological%3ACurrent%3AHourly%3Anombre%40RIO%20SALADO%20EN%20SIFON%20AYQUINA%3Ads)//region/parameter/geoobject/.nombre/.gridvalues/VALUE/info.json"
                     shape="rect"></a>
                  
                  
                  
                  <div class="template" style="color : black">Alerta actual:  <b><span class="iridl:value"></span></b>
                     
                     
                     
                  </div>
                  		
                  
               </div>

               
            </div>
            
            <div class="dlimgtsbox">
               <img class="dlimgts"
                    src="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/Hourly/water_level/T/last/dup/7.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Chile:DGA:hydrological:Current:Hourly:nombre%40RIO%20SALADO%20EN%20SIFON%20AYQUINA:ds%29//region/parameter/geoobject/.nombre/.gridvalues/VALUE/0.0/100.0/masknotrange//fullname/%28Nivel%20del%20r%C3%ADo%20en%20los%20%C3%BAltimos%207%20d%C3%ADas%20%28m%29%29/def/Alert_levels/nombre//region/get_parameter/geoobject/.nombre/.gridvalues/VALUE/a:/.ROJA/T/0.0/mul/add/DATA/0/AUTO/RANGE//fullname/%28Alerta%20Roja%29/def/:a:/.AMARILLA/T/0.0/mul/add//fullname/%28Alerta%20Amarilla%29/def/:a:/.AZUL/T/0.0/mul/add//fullname/%28Alerta%20Temprana%20Preventiva%29/def/:a/4/-1/roll/T/fig-/solid/thinnish/red/line/yellow/line/blue/line/medium/black/line/-fig//plotborderbottom/40/psdef/nombre/last/plotvalue/+.gif" />
               
            </div>
            
         </fieldset>
         
         
         <fieldset class="dlimage" id="content" about="">
            
            <link rel="iridl:hasFigure"
                  href="http://www.climatedatalibrary.cl/expert/SOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.hydrological/.Current/a:/.Hourly/a:/.lon/:a:/.lat/2/copy/:a:/.water_level/:a%5BT/nombre%5DREORDER/CopyStream/0.0/100.0/masknotrange/:a:/.Alert_levels/:a/AZUL/0.0/mul//NO_ALERT/add_variable/%7BNO_ALERT/AZUL/AMARILLA/ROJA%7Dgrouptogrid/-999.0/maskle/sub/0.0/masklt/100.0/div/-1/mul/1/add%5BM%5Ddominant_class//long_name/%28Water%20Level%20Alert%29def/T/last/dup/7/sub/exch/RANGE/startcolormap/transparent/green/green/blue/yellow/red/red/endcolormap/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//bbox%5B-80/-55/-67/-17%5Dpsdef//antialias/true/psdef/T/last/plotvalue/X/-80.04166/-64.95834/plotrange/Y/-56/-16/plotrange//plotaxislength/432/psdef//XOVY/null/psdef//plotborder/72/psdef//plotbordertop/20/psdef//plotborderbottom/60/psdef//framelabel/%28Hora%20y%20fecha:%20%25=%5BT%5D%29psdef/" />
            <img class="dlimg"
                 src="http://www.climatedatalibrary.cl/expert/SOURCES/.WORLDBATH/.bath/X/-80/-65/RANGEEDGES/Y/-16/-56/RANGEEDGES/0/masklt/halfgreyscale/SOURCES/.Chile/.DMC/.Forecasts/.Countrymask/.mask/X/-80/-65/RANGE/Y/-16/-56/RANGE/mul/SOURCES/.Chile/.DGA/.hydrological/.Current/a:/.Hourly/a:/.lon/:a:/.lat/2/copy/:a:/.water_level/:a%5BT/nombre%5DREORDER/CopyStream/0.0/100.0/masknotrange/:a:/.Alert_levels/:a/AZUL/0.0/mul//NO_ALERT/add_variable/%7BNO_ALERT/AZUL/AMARILLA/ROJA%7Dgrouptogrid/-999.0/maskle/sub/0.0/masklt/100.0/div/-1/mul/1/add%5BM%5Ddominant_class//long_name/%28Water%20Level%20Alert%29def/T/last/dup/7/sub/exch/RANGE/startcolormap/transparent/green/green/blue/yellow/red/red/endcolormap/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//bbox%5B-80/-55/-67/-17%5Dpsdef//antialias/true/psdef/T/last/plotvalue/X/-80.04166/-64.95834/plotrange/Y/-56/-16/plotrange//plotaxislength/432/psdef//XOVY/null/psdef//plotborder/72/psdef//plotbordertop/20/psdef//plotborderbottom/60/psdef//framelabel/%28Hora%20y%20fecha:%20%25=%5BT%5D%29psdef/+.gif"
                 border="0"
                 alt="image" />
                 <img src="Caudales_hourly_scale_es" alt="Caudales"> </img>            
         </fieldset>
         
         
       
         
         
         <div id="tabs-1" class="ui-tabs-panel" about="">
            
            <h2 align="center" property="term:title">Alerta de Niveles de R&#237;os</h2>
            
            <p align="left" property="term:description">En este cuarto de mapas se muestra informaci&#243;n sobre crecidas de r&#237;os en Chile.</p>
            
            <p align="left">En el menu>Selecci&#243;on de estaci&#243;n puedes seleccionar la estaci&#243;n de inter&#233;s.</p>
<h3 align="left">Alerta temprana preventiva</h3>
<p align="left">Instancia primaria, que implica la vigilancia permanente de las distintas &#225;reas y escenarios de riesgos.</p>
<h3 align="left">Alerta amarilla</h3>
<p align="left">Se establece cuando la crecida crece en extensi&#243;n y severidad.</p>
<h3 align="left">Alerta roja</h3>
<p align="left">Se establece cuando la crecida llega a una altura donde hay un riesgo por desborde del río.

Una Alerta Roja se puede establecer de inmediato con la amplitud y cobertura necesarias, sin que medie previamente un Alerta Amarilla, seg&#250;n las caracter&#237;sticas de la situaci&#243;n.</p>

<p align="left">Las estaciones de caudales y los niveles de alerta son informados por parte de la Direccion General de Chile (DGA).</p>
    <p align="left">  <img src="../../icons/Logo_dga" alt="Logo DGA" /></p>
            
            
         </div>
         
         
         <div id="tabs-2" class="ui-tabs-panel">
            
            <h2 align="center">Más información</h2>
            
            <p> </p>
            
         </div>
         
         <div id="tabs-3" class="ui-tabs-panel">
            
            <h2 align="center">Dataset Documentation</h2>
            
            <h4><a class="carry" shape="rect">Niveles de agua</a></h4>
            
            <dl class="datasetdocumentation">
               <dt>Data</dt>
               <dd>Nivel de agua en estaciones de la DGA</dd>
               <dt>Data Source</dt>
               <dd>Dirección General de Aguas, <a href="http://www.dga.cl/" shape="rect">DGA</a></dd>
            </dl>
            
         </div>
         
         <div class="ui-tabs-panel-hidden"> 
            
            <h2 align="center">Fuente de los Datos</h2>
            
            <p> <a href="http://www.climatedatalibrary.cl/expert/SOURCES/.Chile/.DGA/.hydrological/.Current/.Hourly/"
                  shape="rect">Accede a la base de datos usado para crear este mapa.</a> 
            </p>
            
         </div>
         
         <div id="tabs-4" class="ui-tabs-panel">
            
            <h2 align="center">Soporte</h2>
            
            <p>
               Sus consultas a: <a href="mailto:mwar_lac@unesco.org?subject=Precipitacion Observada Chile"
                  shape="rect">mwar_lac@unesco.org</a>
               
            </p>
            
         </div>
         
      </div>
      
      <div class="optionsBar">
         
         <fieldset class="navitem" id="share">
            <legend>Share</legend>
         </fieldset>
         
      </div>
      
   </body>
</html>