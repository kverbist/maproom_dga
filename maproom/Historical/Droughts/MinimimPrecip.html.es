<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<meta xml:lang="" property="maproom:Entry_Id" content="Historical" />
<title>Precipitaci&#243;n M&#237;nima Esperada</title>
<link rel="stylesheet" type="text/css" href="../../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="MinimimPrecip.html?Set-Language=en" />
<link class="share" rel="canonical" href="MinimimPrecip.html" />
      <link class="carryLanguage" rel="home" href="http://www.minagri.gob.cl/agroclimatico/unidad_nacional.php/" title="UNEA" />
      <link class="carryLanguage" rel="home alternate" type="application/json"
            href="../../navmenu.json" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Historical_Droughts_term"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
<link rel="term:icon" href="http://www.climatedatalibrary.cl/expert/%285YR_Drought%29//var/parameter/%285YR_Drought%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.5YR_Drought/prcp_fraction_colors/X/Y/fig:/colors/thin/solid/countries_gaz/coasts_gaz/:fig//5YR_Drought/1/2000/plotrange%7Dif//var/get_parameter/%2810YR_Drought%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.10YR_Drought/prcp_fraction_colors/X/Y/fig:/colors/thin/solid/countries_gaz/coasts_gaz/:fig//10YR_Drought/1/2000/plotrange%7Dif//var/get_parameter/%2820YR_Drought%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.20YR_Drought/prcp_fraction_colors/X/Y/fig:/colors/thin/solid/countries_gaz/coasts_gaz/:fig//20YR_Drought/1/2000/plotrange%7Dif//var/get_parameter/%28100YR_Drought%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.100YR_Drought/prcp_fraction_colors/X/Y/fig:/colors/thin/solid/countries_gaz/coasts_gaz/:fig//100YR_Drought/1/2000/plotrange%7Dif/X/-76/-67/plotrange/Y/-55/-14/plotrange/+//XOVY+null+psdef//plotaxislength+432+psdef//plotborder+72+psdef+.gif" />
<script type="text/javascript" src="../../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../../maproom/unesco.js"></script>
</head>
<body  xml:lang="es">
<form name="pageform" id="pageform" class="carryup carry dlimg dlauximg share">
<input class="carryLanguage carryup carry " name="Set-Language" type="hidden" />
<input class="carry dlimg dlauximg dlimgloc admin share" name="bbox" type="hidden" />
<input class="dlimg dlauximg onlyvar share" name="var" type="hidden" />
<input class="dlimg" name="plotaxislength" type="hidden" />
<input class="carry share dlimgloc dlimgts" name="region" type="hidden" />
<input class="carry share dlimgloc dlimgts admin" name="resolution" type="hidden" data-default="irids:SOURCES:Features:Political:Chile:comunas:ds" />
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList"> 
                <legend>Maproom</legend> 
                      <a rev="section" class="navlink carryup" href="/maproom/Historical/">Hist&#243;rica</a>
            </fieldset> 
            <fieldset class="navitem" id="chooseSection"> 
                <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Historical_Droughts_term"><span property="term:label">Atlas de Sequ&#237;as</span></legend>
            </fieldset> 
            <fieldset class="navitem">
                <legend>Región</legend>
                <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsChile.json"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">Chile</option>
                <optgroup class="template" label="Región">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup>
                </select>
            </fieldset>
            <fieldset class="navitem">
               <legend>Análisis</legend><span class="selectvalue"></span><select class="pageformcopy" name="var"><option value="">Periodo de retorno de 5 a&#241;os</option><option value="10YR">Periodo de retorno de 10 a&#241;os</option><option value="15YR">Periodo de retorno de 15 a&#241;os</option><option value="25YR">Periodo de retorno de 25 a&#241;os</option><option value="50YR">Periodo de retorno de 50 a&#241;os</option><option value="75YR">Periodo de retorno de 75 a&#241;os</option><option value="100YR">Periodo de retorno de 100 a&#241;os</option></select>
            
	     </fieldset>
 	     <fieldset class="navitem"><legend>Promedio espacial sobre</legend><span class="selectvalue"></span><select class="pageformcopy" name="resolution">
		<option value="0.0125">Ubicación puntual</option>
		<option value="irids:SOURCES:Features:Political:Chile:provincias:ds">Provincia</option>
		<option value="irids:SOURCES:Features:Political:Chile:comunas:ds">Comuna</option>
		<option value="irids:SOURCES:Features:Political:Chile:distritos:ds">Localidad</option>
		</select>
    		<link class="admin" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/%28irids:SOURCES:Features:Political:Chile:comunas:ds%29//resolution/parameter/geoobject/%28bb:-80:-55:-67:-17:bb%29//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
          	<select class="pageformcopy" name="region">
            	<optgroup class="template" label="Label">
            	<option class="iridl:values region@value label"></option>
              </optgroup>
            	</select>
            </fieldset>	
 </div>
<div class="ui-tabs">
    <ul class="ui-tabs-nav">
      <li><a href="#tabs-1" >Descripci&#243;n</a></li>
      <li><a href="#tabs-2" >M&#225;s informaci&#243;n</a></li>
      <li><a href="#tabs-3" >Fuente</a></li>
      <li><a href="#tabs-4" >Soporte</a></li>
      <li><a href="#tabs-5" >Instrucciones</a></li>
    </ul>

<fieldset class="dlimage regionwithinbbox">
<a class="dlimgts" rel="iridl:hasTable" href="http://www.climatedatalibrary.cl/expert/SOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/%7B5YR/10YR/15YR/25YR/50YR/75YR/100YR%7Dgrouptogrid//long_name/%28Precipitacion%20Minima%20Esperada%29def//units/%28mm/year%29def/M/%28Periodo%20de%20Retorno%29renameGRID/%28bb:-72.04375%2C-38.04375%2C-72.03125%2C-38.03125%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/DATA/0/AUTO/RANGE/table:/1/:table/"></a>

<div style="float: left;">
<img class="dlimgloc" src="http://www.climatedatalibrary.cl/expert/SOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.5YR/X/Y/%28bb:-76%2C-55.0%2C-67%2C-16.0%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-71.0166%2C-30.0166%2C-71.0%2C-30.0%29/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborder/0/psdef//plotaxislength/120/psdef/+.gif" />
</div>	

<div style="float: left;">

<div class="valid" style="display: inline-block; text-align: top;">
<a class="dlimgts" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json"></a>
<div class="template"> Observaciones para <span class="bold iridl:long_name"></span>
</div>
</div>

<div class="valid" style="text-align: top;">
<a class="dlimgloc" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/%7B%7D%7B%28bb:-72.04375%2C-38.04375%2C-72.03125%2C-38.03125%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/toi4/name/cvntos/%28YR%29search/pop/nip/nip//long_name/exch/def%7Dforalldatasets2/%7Ba7/a2/a3/a4/a5/a1/a6%7Dds/info.json"></a>
<script type="application/json" property="iridl:hasPUREdirective">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "+td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"

}
}
}
</script>
                  <div>
                     <table class="valid template">
                        <tr style="color : black">
                           <td rowspan="1" colspan="1">Precipitaci&#243;n M&#237;nima Esperada para un Periodo de Retorno de</td>
                           <td class="name " rowspan="1" colspan="1"> a&#241;os</td>
                           <td align="right" class="value " rowspan="1" colspan="1"> mm</td>
                        </tr>
                     </table>
                     
                  </div>


</div>
</div>


<br />
<img class="dlimgts regionwithinbbox" src="http://www.climatedatalibrary.cl/expert/SOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/%7B5YR/10YR/15YR/25YR/50YR/75YR/100YR%7Dgrouptogrid//long_name/%28Precipitation%29def//units/%28mm/anio%29def/M/%28Periodo_de_Retorno%29renameGRID/%28bb:-72.04375%2C-38.04375%2C-72.03125%2C-38.03125%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/DATA/0/AUTO/RANGE/dup/Periodo_de_Retorno/fig-/colorbars2/-fig//plotborder/72/psdef//plotaxislength/432/psdef/+//plotborder+72+psdef//plotaxislength+432+psdef+.gif" />
</fieldset>

<fieldset class="dlimage" id="content" about="">
<link rel="iridl:hasFigure" href="http://www.climatedatalibrary.cl/expert/%285YR%29//var/parameter/%285YR%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.5YR//long_name/%28Precipitacion%20Minima%20Esperada%20cada%205%20anios%29def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig%7Dif//var/get_parameter/%2810YR%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.10YR//long_name/%28Precipitacion%20Minima%20Esperada%20cada%2010%20anios%29def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig%7Dif//var/get_parameter/%2815YR%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.15YR//long_name/%28Precipitacion%20Minima%20Esperada%20cada%2015%20anios%29def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig%7Dif//var/get_parameter/%2825YR%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.25YR//long_name/%28Precipitacion%20Minima%20Esperada%20cada%2025%20anios%29def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig%7Dif//var/get_parameter/%2850YR%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.50YR//long_name/%28Precipitacion%20Minima%20Esperada%20cada%2050%20anios%29def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig%7Dif//var/get_parameter/%2875YR%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.75YR//long_name/%28Precipitacion%20Minima%20Esperada%20cada%2075%20anios%29def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig%7Dif//var/get_parameter/%28100YR%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.100YR//long_name/%28Precipitacion%20Minima%20Esperada%20cada%20100%20anios%29def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig%7Dif/X/-76/-67/plotrange/Y/-55/-16/plotrange/%28antialias%29true/psdef/%28fntsze%2920/psdef/" />
<img class="dlimg" src="http://www.climatedatalibrary.cl/expert/%285YR%29//var/parameter/%285YR%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.5YR//long_name/%28Precipitacion%20Minima%20Esperada%20cada%205%20anios%29def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig%7Dif//var/get_parameter/%2810YR%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.10YR//long_name/%28Precipitacion%20Minima%20Esperada%20cada%2010%20anios%29def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig%7Dif//var/get_parameter/%2815YR%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.15YR//long_name/%28Precipitacion%20Minima%20Esperada%20cada%2015%20anios%29def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig%7Dif//var/get_parameter/%2825YR%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.25YR//long_name/%28Precipitacion%20Minima%20Esperada%20cada%2025%20anios%29def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig%7Dif//var/get_parameter/%2850YR%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.50YR//long_name/%28Precipitacion%20Minima%20Esperada%20cada%2050%20anios%29def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig%7Dif//var/get_parameter/%2875YR%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.75YR//long_name/%28Precipitacion%20Minima%20Esperada%20cada%2075%20anios%29def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig%7Dif//var/get_parameter/%28100YR%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.100YR//long_name/%28Precipitacion%20Minima%20Esperada%20cada%20100%20anios%29def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig%7Dif/X/-76/-67/plotrange/Y/-55/-16/plotrange/%28antialias%29true/psdef/%28fntsze%2920/psdef/++Y/-55.96674/-17.5/plotrange+//plotborder+72+psdef//plotaxislength+432+psdef+.gif"  border="0" alt="image" /><br />
<img class="dlauximg" src="http://www.climatedatalibrary.cl/expert/%285YR%29//var/parameter/%285YR%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.5YR//long_name/%28Precipitacion%20Minima%20Esperada%20cada%205%20anios%29def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig%7Dif//var/get_parameter/%2810YR%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.10YR//long_name/%28Precipitacion%20Minima%20Esperada%20cada%2010%20anios%29def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig%7Dif//var/get_parameter/%2815YR%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.15YR//long_name/%28Precipitacion%20Minima%20Esperada%20cada%2015%20anios%29def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig%7Dif//var/get_parameter/%2825YR%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.25YR//long_name/%28Precipitacion%20Minima%20Esperada%20cada%2025%20anios%29def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig%7Dif//var/get_parameter/%2850YR%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.50YR//long_name/%28Precipitacion%20Minima%20Esperada%20cada%2050%20anios%29def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig%7Dif//var/get_parameter/%2875YR%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.75YR//long_name/%28Precipitacion%20Minima%20Esperada%20cada%2075%20anios%29def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig%7Dif//var/get_parameter/%28100YR%29eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Chile/.MinExpectedPrecip/.100YR//long_name/%28Precipitacion%20Minima%20Esperada%20cada%20100%20anios%29def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig%7Dif/X/-76/-67/plotrange/Y/-55/-16/plotrange/%28antialias%29true/psdef/%28fntsze%2920/psdef/.auxfig+Y/-55.96674/-17.5/plotrange+//plotborder+72+psdef//plotaxislength+432+psdef+.gif" />
</fieldset>
 <div id="tabs-1" class="ui-tabs-panel" about="">
<h3 align="center"  property="term:title" >Precipitaci&#243;n M&#237;nima Esperada</h3>
<p align="left" property="term:description">Este mapa muestra la precipitaci&#243;n m&#237;nima anual esperada para m&#250;ltiples periodos de retorno para Chile.</p>
<p align="left">En el men&#250;>an&#225;lisis puedes seleccionar el periodo de retorno: 5, 10, 25, 50, 75 o 100 a&#241;os. En el men&#250;>regi&#243;n puedes seleccionar la regi&#243;n de inter&#233;s.</p>
<p align="left">Los diferentes periodos de retorno indican la recurrencia de un evento extremo. Por ejemplo, si el mapa indica que por un periodo de retorno de 5 a&#241;os la precipitaci&#243;n m&#237;nima esperada es 100 mm significa que en ese punto se puede esperar una precipitaci&#243;n anual de 100 mm cada 5 a&#241;os.</p>
<p>El m&#233;todo usado es un an&#225;lisis de frecuencia de eventos extremos hist&#243;ricos. Significa que se usa datos de precipitaci&#243;n hist&#243;ricos para ajustar una distribuci&#243;n probabil&#237;stica. Esa distribuci&#243;n probabil&#237;stica es usada para identificar la frecuencia con la cual esperamos tener eventos con una cierta magnitud. </p>
<p align="left">El atlas de sequ&#237;as es proporcionado por el Centro del Agua para Zonas &#193;ridas en Am&#233;rica Latina y el Caribe  (CAZALAC).</p>
<p align="left">  <img src="../../icons/Logo_cazalac" alt="Logo CAZALAC"></img></p>
<p align="left"><b>Referencias</b> </p>
<p align="left">Nunez, J.H., K. Verbist, J. Wallis, M. Schaeffer, L. Morales, and W.M. Cornelis. 2011. Regional frequency analysis for mapping drought events in north-central Chile. <i>J. Hydrol. </i> <b>405</b> 352-366.
</p>
</div>

<div id="tabs-2"  class="ui-tabs-panel">
<h3  align="center">&#191;Qu&#233; m&#233;todo fue usada para calcular la precipitaci&#243;n m&#237;nima esperada?</h3>
<p>
El m&#233;todo usado toma en consideraci&#243;n la baja intensidad de datos, los datos son agrupados en regiones que son climatol&#243;gicamente homog&#233;neos cual permite de aplicar estad&#237;sticas m&#225;s robustos.
 </p>
<p>Para poder debilitar el efecto de eventos extremos se usa L-momentos en vez de momentos normales. De esta forma eventos extremos no influencian la selecci&#243;n de la distribuci&#243;n. Este m&#233;todo es seleccionado por que es m&#225;s apropiado en regiones con una variabilidad interanual significativa y series de datos de corta duraci&#243;n.</p>
<p>Informaci&#243;n detallada sobre el m&#233;todo usado se puede encontrar <a href="http://www.cazalac.org/documentos/Guia_Metodologia_Atlas_de_Sequia.pdf">aqu&#237;</a>.</p> 
</div>


<div id="tabs-3" class="ui-tabs-panel">
<h3  align="center">Fuente de los Datos</h3>
<p><a href="http://climatedatalibrary.cl/SOURCES/.CAZALAC/.DroughtAtlas/.Chile/">Atlas de Sequ&#237;a de America Latina y el Caribe</a>, entregado por el Centro <a href="http://www.climatedatalibrary.cl/SOURCES/.CAZALAC/.DroughtAtlas/">CAZALAC</a></p>
</div>

<div id="tabs-4"  class="ui-tabs-panel">
<h3  align="center">Soporte</h3>
<p>
Sus consultas a: <a href="mailto:mwar_lac@unesco.org?subject=Precipitacion Observada Chile">mwar_lac@unesco.org</a>
 </p>
</div>
<div id="tabs-5"  class="ui-tabs-panel">
<h3  align="center">Instrucciones</h3>
<div class="buttonInstructions"></div>
</div>
</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend></fieldset>
</div>
 </body>
 </html>
