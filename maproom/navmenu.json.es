{
"@context": {
"links": { "@container": "@list" },
"groups": {}
},
"options": [
{"href": "http://www.dga.cl/",
 "title": "Direccion General de Aguas"}
],
"groups": [
{
"title": "Developped with support from",
"links": [
{"href": "http://www.unesco.org/new/en/santiago/natural-sciences/hydrological-systems-and-global-change/",
 "title": "UNESCO"},
{"href": "http://www.rlc.fao.org/es/",
 "title": "FAO"},
{"href": "http://iri.columbia.edu/",
 "title": "International Research Institute"},
{"href": "http://www.cazalac.org/",
"title": "Centro del Agua para Zonas Aridas"}
]
}
]
}